﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XmlPamoka
{
    public class Asmuo
    {
        public int Id { get; set; }

        public string Vardas { get; set; }

        public string Pavarde { get; set; }

        public int Amzius { get; set; }

        public _Adresas Adresas { get; set; }

        public class _Adresas
        {
            public string Miestas { get; set; }

            public string Gatvė { get; set; }

            public string NamoNr { get; set; }
        }
    }
}
