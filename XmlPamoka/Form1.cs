﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace XmlPamoka
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            //XmlDocument document = new XmlDocument();
            //XmlElement root = document.CreateElement("root");
            //document.AppendChild(root);
            //XmlElement asmuo = document.CreateElement("asmuo");
            //asmuo.SetAttribute("AkiųSpalva", "Ruda");
            //asmuo.SetAttribute("Ugis", "174");
            //root.AppendChild(asmuo);
            //XmlNode vardas = document.CreateElement("vardas");
            //vardas.InnerText = "Jonas";
            //asmuo.AppendChild(vardas);


            //if (document.SelectSingleNode("root/asmuo/vardas") != null)
            //{
            //    textBox1.Text = document.SelectSingleNode("root/asmuo/vardas").InnerText;
            //} else
            //{
            //    textBox1.Text = "-";
            //}

            //var filename = $"{document.SelectSingleNode("root/asmuo/vardas").InnerText}_duomenys.xml";
            //document.Save(AppDomain.CurrentDomain.BaseDirectory + "../../ZmoniųDuomenys/" + filename);
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtVardas.Text))
            {
                var asmuo = new Asmuo
                {
                    Id = -1,
                    Vardas = txtVardas.Text,
                    Pavarde = txtPavarde.Text,
                    Amzius = int.Parse(txtAmzius.Text),
                    Adresas = new Asmuo._Adresas()
                    {
                        Miestas = txtMiestas.Text,
                        Gatvė = txtGatve.Text,
                        NamoNr = txtNamonr.Text
                    }
                };

                XmlDocument document = new XmlDocument();
                XmlElement root = document.CreateElement("root");
                document.AppendChild(root);
                XmlElement vardas = document.CreateElement("vardas");
                vardas.InnerText = asmuo.Vardas;
                root.AppendChild(vardas);
                XmlElement pavarde = document.CreateElement("pavarde");
                pavarde.InnerText = asmuo.Pavarde;
                root.AppendChild(pavarde);
                XmlElement amzius = document.CreateElement("amzius");
                amzius.InnerText = asmuo.Amzius.ToString();
                root.AppendChild(amzius);
                XmlElement adresas = document.CreateElement("adresas");
                root.AppendChild(adresas);
                XmlElement miestas = document.CreateElement("miestas");
                miestas.InnerText = asmuo.Adresas.Miestas;
                adresas.AppendChild(miestas);
                XmlElement gatvė = document.CreateElement("gatve");
                gatvė.InnerText = asmuo.Adresas.Gatvė;
                adresas.AppendChild(gatvė);
                XmlElement namonr = document.CreateElement("namoNr");
                namonr.InnerText = asmuo.Adresas.NamoNr;
                adresas.AppendChild(namonr);

                string folder = AppDomain.CurrentDomain.BaseDirectory + "../../ZmoniųDuomenys/";
                var filename = $"{asmuo.Vardas}_duomenys.xml";
                document.Save(folder + filename);
            }
            else
            {
                MessageBox.Show("Neivedete vardo.");
            }
            
        }
    }
}
